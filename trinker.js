const { forEach, sort } = require("./people");
const people = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        return p.filter (person => person.gender ==='Male')
    },

    allFemale: function(p){
        return p.filter (person => person.gender ==='Female')
    },

    nbOfMale: function(p){
        return this.allMale(p).length
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length
    },

    nbOfMaleInterest: function(p){
        return p.filter (person => person.looking_for ==='M').length

    },

    nbOfFemaleInterest: function(p){
        return p.filter (person => person.looking_for ==='F').length
    },
    salaire: function(p){
        return p.filter (person => parseInt(person.income.substring(1) )> 2000).length

    },
    drame: function(p){
        return p.filter (person=>person.pref_movie.includes('Drama')).length
    },
    sf_female:function(p){
        return p.filter (
            person => person.pref_movie.includes('Sci-Fi') && person.gender === 'Female' 
        ).length
    },
    docu_smic:function(p){
        return p.filter(
             person => parseInt(person.income.substring(1)) > 1482 && 
             person.pref_movie.includes('Documentary') 
        ).length
    },
    cadre_supp:function(p){
    let array =[]
    let cadre =  p.filter (person => parseInt(person.income.substring(1) )> 4000)
    cadre.forEach(element => {
    array.push(element.id +" "+ element.first_name +" "+ element.last_name +" gagne "+ element.income.substring(1)+" par mois")
  });
    return array    
    },
   
    musk: function(p){
        let max = 0;
        let elon;
        this.allMale(p).forEach(person => {
            let income = parseFloat(person.income.substring(1));
            if(income > max) {
                max = income;
                elon = person; 
            }
        }); 
        return elon.id  + ' ' + elon.last_name + ' ' + elon.first_name + " " + max ;
    },
    moyenne:function(p){
      
        let id =[]
        let sum = []
        let moy =  p.filter (person => parseFloat(person.income.substring(1) ))
      moy.forEach(element =>{
         sum.push( parseFloat(element.income.substring(1) ))
      })
         moy.forEach(element =>{
            id.push( parseFloat(element.id ))
      })
         let red = (first,seconde)=>first + seconde
         
        return   sum.reduce(red)/id.length
        
    },
     median:function(p){
        let med = []
       const salair = p.filter (person => parseFloat(person.income.substring(1) )>0)
        salair.forEach(element =>{
            med.push( parseFloat(element.income.substring(1) ))
            med.sort((a, b) => a - b);
            mediane = (med[med.length/2] + med[(med.length/2)-1])/2
         })
      return mediane

    },

     hemisNord:function(p){
        return p.filter (person=>person.latitude > 0).length
    },

     salairSud:function(p){
        let id = []
        let salsud =[]
        let sud = p.filter (person=>person.latitude < 0)

        sud.forEach(element =>{
        salsud.push( parseFloat(element.income.substring(1) ))
     })
     sud.forEach(element =>{
        id.push( parseFloat(element.id ))

     })
     let total = (a,b) => a+b
     let sudsal = salsud.reduce(total)/id.length
     return sudsal
    },

    

    berenice: function (p){
        Cawt = p.filter(cawt =>  cawt.last_name == "Cawt");
        other = p.filter(cawt =>  cawt.last_name !== "Cawt");
       

         function Rad(Value) {
            
            return Value * Math.PI / 180;
        }
        
        function position(lat1,lat2,lng1,lng2){
            globrad = 6372.8; 
            deltaLat = Rad(lat2-lat1);
            deltaLng = Rad(lng2-lng1);
            lat1 = Rad(lat1);
            lat2 = Rad(lat2);
            a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return  globrad * c;

        }


        function calculate(){
            let per;
            var result = position(Cawt[0].latitude,((Cawt[0].latitude) + 90),Cawt[0].longitude,((Cawt[0].longitude) + 90));
            for (var i=0;i<other.length;i++){ 
            var ans = position(Cawt[0].latitude,other[i].latitude,Cawt[0].longitude,other[i].longitude);
            if (ans < result){
            result = ans;
            per =other[i];
                    
                }       
            }
            return `${per.first_name} ${per.last_name} se trouve a ${result.toFixed(2)} km et son id est ${per.id} ` ;
        }

        return calculate()
    },

    brach:function(p){

       rui = p.filter(rui=>  rui.last_name == "Brach");
       autre = p.filter(rui =>  rui.last_name !== "Brach");

        function Rad2(Value) {
            
            return Value * Math.PI / 180;
        }

         function positionRui(lat1,lat2,lgn1,lgn2){
            globrad = 6372.8; 
            deltaLat = Rad2(lat2-lat1);
            deltaLng = Rad2(lgn2-lgn1);
            lat1 = Rad2(lat1);
            lat2 = Rad2(lat2);
            a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.sin(deltaLng/2) * Math.sin(deltaLng/2) * Math.cos(lat1) * Math.cos(lat2); 
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return  globrad * c;


         }

        function calculate(){
            let per;
            var result = positionRui(rui[0].latitude,((rui[0].latitude) + 90),rui[0].longitude,((rui[0].longitude) + 90));

            for (var i=0;i<autre.length;i++){ 
            var ans = positionRui(rui[0].latitude,autre[i].latitude,rui[0].longitude,autre[i].longitude);
            if (ans < result){
            result = ans;
            per =autre[i];
                  
                }       
            }
            return `${per.first_name} ${per.last_name} se trouve a ${result.toFixed(2)} km et son id est ${per.id} ` ;
        }

            return calculate()
   

    },

Boshard:function(p){

},
google:function(p){
    let list = []
    let google_employer = p.filter (person=>person.email.includes('google'))
google_employer.forEach(element=> {
list.push(element.id +" "+ element.first_name +" "+ element.last_name)
})
return list
},

 
doyen:function(p){
    let max = p.length;
    let calment;
    this.allMale(p).forEach(person => {
        let birth= parseFloat(person.date_of_birth.substring(1));
        
        if(birth < max) {
            max = birth;
            calment = person; 
        }
    }); 
    return calment.id  + ' ' + calment.last_name + ' ' + calment.first_name + " " + calment.date_of_birth ;
},
plusjeune:function(p){
    let max = 0;
    let junior;
    this.allMale(p).forEach(person => {
        let birthYoung= parseFloat(person.date_of_birth.substring(1));
        
        if(birthYoung > max) {
            max = birthYoung;
            junior = person; 
        }
    }); 

    return junior.id  + ' ' + junior.last_name + ' ' + junior.first_name + " " + junior.date_of_birth ;
},

difference_age:function(p){
   let sumdiff_age =[]
   p.forEach(element => {
       sumdiff_age.push(parseFloat (element.date_of_birth))
   }) 
let totalDiff = (a,b) => (a-b)

 let moyDiff = sumdiff_age.reduce(totalDiff)/sumdiff_age.length

return moyDiff
},

pop_movie:function(p){
   

},
film_noirs:function(p){
let sumAge = []
const today = 2021

         let Fnoirs = p.filter (person => person.pref_movie.includes('Film-Noir') && person.gender === 'Male')
        Fnoirs.forEach(element =>{
            sumAge.push( parseFloat(element.date_of_birth ))
         })
//for(let i =0 ; i <sumAge.length ; i++){
    let total = (a,b) => a+b
     let moye = ((today * sumAge.length) - sumAge.reduce(total))/sumAge.length 
     return parseInt (moye) + ' ans.'






},





    match : function(p){
        return "not implemented".red;
    }
    }